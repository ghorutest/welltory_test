#!/usr/bin/python3
import json
import jsonschema
from jsonschema import validate
from pathlib import Path

events_folder = Path.cwd().joinpath('task_folder', 'event')
schema_folder = Path.cwd().joinpath('task_folder', 'schema')
schemas_list = {}


def validate_json(data):
    event_type = str(data['event'])
    print('Event type in JSON: \'' + event_type + '\'')
    try:
        validate(instance=data['data'], schema=schemas_list[data['event']])
        print('(OK) JSON data is valid, used schema: ' + event_type)
    except KeyError as err:
        print('(!!!) Schema fo type ' + str(err) + ' not found')
    except jsonschema.exceptions.ValidationError as err:
        print('(!!!) Error: ' + str(err).splitlines()[0])
        print('Required keys: ' + str([data_item for data_item in schemas_list[data['event']]['required']]))
        print('Founded in JSON keys: ' + str([data_item for data_item in data['data']]))


def read_schemas():
    for file in schema_folder.glob('*.schema'):
        with open(file, "r") as schema_file:
            schemas_list[file.stem] = json.load(schema_file)


def read_events_list():
    for file in events_folder.glob('*.json'):
        with open(file, "r") as json_file:
            print("\nValidating file: " + json_file.name)
            try:
                validate_json(json.load(json_file))
            except (TypeError, KeyError):
                print('(!!!) File reading error, check file contents')


read_schemas()
read_events_list()
